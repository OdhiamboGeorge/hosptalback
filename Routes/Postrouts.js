const express = require("express");
const route = express.Router();

const controler = require("../Controles/Postrequest");

route.post("/order", controler.storeorder);
route.post("/addmeal", controler.addmeal);
route.post("/login", controler.login);
route.post("/creatuser", controler.creatuser);
route.post("/depatment",controler.depatments);
route.post("/addrole", controler.frontdesck);

module.exports = route;

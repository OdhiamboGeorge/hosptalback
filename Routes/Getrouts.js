const express = require("express");
const route = express.Router();

const control = require("../Controles/Getrequest");

route.get("/order", control.getorders);
route.get("/menue",control.getmeals);
route.get("/depatments",control.getdepatments);
route.get("/frontdesck",control.getFrontdesck);

module.exports = route;

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const usershema = new Schema({
    email: {
        type: String,
        required: true
    },
    role:{
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    }
},{
    timestamps: true
});


const foodshema = new Schema({
    name: {
        type: String,
        required: true
    },
    discription: {
        type: String,
        required: true
    },
    imgurl: {
        type: String,
        required: true
    },
    price:{
        type: Number,
        required: true
    }
});


const reservhema = new Schema({
    name: {
        type: String,
        required: true
    },
    discription: {
        type: String,
        required: true
    },
    imgurl: {
        type: String,
        required: true
    },
    price:{
        type: Number,
        required: true
    }
});


const orderchema = new Schema({
    name:{
        type: String,
        required: true
    },
    shipTo:{
        type: String,
        required: true
    },
    paymentMethod:{
        type: String,
        required: true
    },
    amount:{
        type: Number,
        require: true
    }
},{
    timestamps: true
});

const depatment = new Schema({
    sms:{
        type: String,
        required: true
    },
   ava: {
       type: String,
       required: true
   },
  title: {
      type: String,
      required: true
  },
  imgurl: {
      type: String,
      required: true
  }},{
    timestamps: true
}) 

const frontdesck = new Schema({
    task:{
        type: String,
        required: true
    },
   ava: {
       type: String,
       required: true
   },
  title: {
      type: String,
      required: true
  }},{
    timestamps: true
}) 

const Order = mongoose.model('Orders',orderchema);
const reservation = mongoose.model('Resarvations',reservhema);
const food = mongoose.model('Foods',foodshema);
const User = mongoose.model('User',usershema);
const Depatment = mongoose.model('Depatment',depatment);
const Frontdesck = mongoose.model('Frontdesck',frontdesck);




module.exports = {
    User: User,
    Food: food,
    Reservation: reservation,
    Order: Order,
    Depatment:Depatment,
    Frontdesck: Frontdesck
}
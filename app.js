const express = require("express");
const bodyparser = require("body-parser");
const mongoose = require("mongoose");
require("dotenv").config();

const route1 = require("./Routes/Getrouts");
const route2 = require("./Routes/Postrouts");

const app = express();
const PORT = process.env.PORT || 8080;

app.use(bodyparser.json());
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

app.use(route1);
app.use(route2);

mongoose
.connect(process.env.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then((result) => {
    app.listen(PORT,console.log("Cliant Connected"));
  })
  .catch((err) => {
    console.log("err while connecting to DB");
  });

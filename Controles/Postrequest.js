const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const {
  User,
  Food,
  Reservation,
  Order,
  Depatment,
  Frontdesck,
} = require("../DBschema/Schema");

const comparepass = async (inputpassword, dbpassword) => {
  return bcrypt.compare(inputpassword, dbpassword);
};

exports.storeorder = async (req, res, next) => {
  try {
    const order = new Order({
      name: req.body.name,
      shipTo: req.body.shipTo,
      paymentMethod: req.body.paymentMethod,
      amount: req.body.amount,
    });

    const result = await order.save();

    res.status(201).json({
      order: result,
    });
  } catch (error) {
    if (!err.statusCode) {
      err.statusCode = 500;
    }
    console.log("Error while feaching single post " + error);
    res.status(500).json({
      massage: "Cant find the request",
    });
  }
};

exports.login = async (req, res) => {
  try {
    const inputpassword = req.body.password;
    let user;
    const result = await User.findOne({ email: req.body.email });
    if (!result) {
      const error = new Error("User not found");
      error.statusCode = 401;
      throw error;
    }
    user = result;
    const passequal = await comparepass(inputpassword, result.password);

    if (!passequal) {
      const error = new Error("Incorect password or Email");
      error.statuscode = 401;
      throw error;
    }

    const token = jwt.sign(
      {
        email: user.email,
        userId: user._id.toString(),
      },
      process.env.SECREAT,
      {
        expiresIn: "1h",
      }
    );

    res.status(200).json({
      token: token,
      aurth: true,
      userId: user._id.toString(),
    });
  } catch (error) {
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    console.log("An error has occured" + error);
    res.status(500).json({ massage: "Login error" });
  }
};

exports.addmeal = async (req, res) => {
  try {
    const meal = new Food({
      name: req.body.name,
      discription: req.body.discription,
      imgurl: req.body.imgurl,
      price: req.body.price,
    });

    const result = await meal.save();

    res.send({
      result,
    });
  } catch (error) {
    res.status(500).json({
      massage: "meal not created",
    });
  }
};

exports.frontdesckreservation = async (req, res) => {
  try {
    const result = await Reservation.find({ email: req.body.email });
    if (!result) {
      console.log("User farst time");
    }
    const reserve = new Reserve({
      name: req.body.name,
      email: req.body.email,
      time: req.body.time,
      tableNo: req.body.tableNo,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      massage: "error",
    });
  }
};

exports.creatuser = async (req, res) => {
  try {
    const cheackuser = await User.findOne({ email: req.body.email });

    if (cheackuser) {
      res.status(200).json({
        massage: "Email Already Registerd",
      });
    } else {
      const hashedpassword = await bcrypt.hashSync(req.body.password, 10);
      const user = new User({
        email: req.body.email,
        name: req.body.name,
        role: req.body.role,
        password: hashedpassword,
      });

      const result = await user.save();
      res.status(201).json({
        message: "User Created",
        userId: result._id,
      });
    }
  } catch (error) {
    res.status(500).json({
      message: "User Not Created",
      error,
    });
  }
};

exports.depatments = async (req, res) => {
  try {
    const depatment = new Depatment({
      sms: req.body.sms,
      ava: req.body.ava,
      title: req.body.title,
      imgurl: req.body.imgurl,
    });

    const result = await depatment.save();
    res.status(200).json({
      result,
    });
  } catch (error) {
    res.status(500).json({
      massage: "Depatment not created"
    })
  }
};

exports.frontdesck = async (req, res) => {
  try {
    const role = new Frontdesck({
      task: req.body.task,
      ava: req.body.ava,
      title: req.body.title,
    });

    const result = await role.save();
    res.status(200).json({
      result,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      massage: "error has occured",
    });
  }
};

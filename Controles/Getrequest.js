const { Order,Depatment,User,Frontdesck } = require("../DBschema/Schema");


exports.getorders = async (req,res,next) => {
    
    try {
        const result = await Order.find()
        
        if (!result) {
            const error = new Error('No orders found');
            error.statusCode = 401;
            throw error;
        }
        
        res.status(200).json(result);

    } catch (error) {
        if (!err.statusCode) {
            err.statusCode = 500;
        }
        console.log('Error while feaching single post '+ error);
        res.status(500).json({
            massage: 'Cant find the request'
        })
    }
}

exports.getmeals = async (req,res) => {
    try {
        const result = await Meal.find()

        res.status(200).json(result);

    } catch (error) {
        res.status(200).json(error);
    }
}

exports.getdepatments = async (req,res) => {
    try {
        const result = await Depatment.find()

        res.status(200).json(result);

    } catch (error) {
        res.status(200).json(error);
    }
}

exports.getUser = async (req,res) => {
    try {
        const result = await User.findById(req.body.userId)
        res.status(200).json(result)
    } catch (error) {
        res.status(500).json({massage:"An Error"})
        console.log(error);
    }
}

exports.getFrontdesck = async (req,res) => {
    try {
        const result = await Frontdesck.find()
        res.status(200).json(result)
    } catch (error) {
        res.status(500).json({
            massage: "Error in front Desck"
        })
    }
}